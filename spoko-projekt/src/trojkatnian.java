
import java.util.HashSet;
import java.util.Set;

public class trojkatnian {

    int licz() {
        //podzial na pojedyncze slowa
        String slowa = (new fileToStr("slowa.txt","")).toString();
        slowa = slowa.substring(1, slowa.length() - 1);
        String[] listaSlow = slowa.split("\",\"");

        //liczenie sum slow oraz wybranie maksymalnej sumy
        int maksSumaSlowa = 0;
        int[] sumySlow = new int[listaSlow.length];
        for (int j = 0; j < listaSlow.length; j++) {
            int sumaSlowa = 0;
            for (int i = 0; i < listaSlow[j].length(); i++) {
                sumaSlowa += listaSlow[j].charAt(i) - 64;
            }
            sumySlow[j] = sumaSlowa;
            maksSumaSlowa = Math.max(maksSumaSlowa, sumaSlowa);
        }

        //liczenie liczb trojkatnych
        Set liczbyTrojkatne = new HashSet();
        int liczbaTrojkatna = 0;
        int n = 0;
        while (liczbaTrojkatna < maksSumaSlowa) {
            liczbaTrojkatna = n * (n + 1) / 2;
            liczbyTrojkatne.add(liczbaTrojkatna);
            n++;
        }

        //sprawdzanie ile jest slow trojkatnych
        int sumaSlowTrojkatnych = 0;
        for (int sumaSlowa : sumySlow) {
            if (liczbyTrojkatne.contains(sumaSlowa)) {
                sumaSlowTrojkatnych++;
            }
        }

        return sumaSlowTrojkatnych;
    }
  

}
