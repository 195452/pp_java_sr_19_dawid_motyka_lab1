
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dawid
 */
public class fileToStr {

    String slowa;

    public fileToStr(String nazwaPliku, String zamiastNewline) {
        slowa = "";
        FileReader plik;
        try {
            plik = new FileReader(nazwaPliku);
            BufferedReader in;
            in = new BufferedReader(plik);
            while (in.ready()) {
                slowa += in.readLine();
                slowa += zamiastNewline;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.toString());
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }

    }
    
    @Override public String toString()
    {
        return slowa;
    }
    
}
